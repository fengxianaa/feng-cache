package com.feng.cache.test;

import com.alibaba.fastjson.JSONObject;
import com.feng.cache.annotation.CacheEvict;
import com.feng.cache.annotation.Cacheable;
import com.feng.cache.annotation.CaffeineCache;
import com.feng.cache.annotation.RedisCache;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@Service
public class UserService {

    @Cacheable(key="#id",ignoreException = false,
            CaffeineCache = @CaffeineCache(timeUnit = TimeUnit.SECONDS),
            RedisCache = @RedisCache(expireTime = 60,timeUnit = TimeUnit.SECONDS))
    public User getById(long id){
        User user = new User();
        user.setId(id);
        user.setName("feng");
        user.setAge(18);
        System.out.println(String.format("直接查询数据库:{%s}", JSONObject.toJSONString(user)));
        return user;
    }

    @Cacheable(value = "fengxainsheng",ignoreException = false,
        CaffeineCache = @CaffeineCache(timeUnit = TimeUnit.SECONDS),
        RedisCache = @RedisCache(expireTime = 60,timeUnit = TimeUnit.SECONDS))
    public User getByIdAndName(long id,String name){
        User user = new User();
        user.setId(id);
        user.setName(name);
        user.setAge(18);
        System.out.println(String.format("直接查询数据库:{%s}", JSONObject.toJSONString(user)));
        return user;
    }

    @Cacheable(value = "fengxainsheng",ignoreException = false,
            CaffeineCache = @CaffeineCache(timeUnit = TimeUnit.SECONDS),
            RedisCache = @RedisCache(expireTime = 60,timeUnit = TimeUnit.SECONDS))
    public User getByObj(User user){
        user.setAge(100);
        System.out.println(String.format("直接查询数据库:{%s}", JSONObject.toJSONString(user)));
        return user;
    }

    @CacheEvict(key="#id")
    public void deleteById(long id){

    }
}
