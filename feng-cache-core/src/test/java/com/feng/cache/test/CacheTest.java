package com.feng.cache.test;

import com.alibaba.fastjson.JSONObject;
import com.feng.cache.config.CacheConfig;
import com.feng.cache.config.RedisConfig;
import com.feng.cache.key.SimpleKey;
import com.feng.cache.listener.CacheMessage;
import com.feng.cache.support.Constants;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

// SpringJUnit4ClassRunner再Junit环境下提供Spring TestContext Framework的功能。
@RunWith(SpringJUnit4ClassRunner.class)
// @ContextConfiguration用来加载配置ApplicationContext，其中classes用来加载配置类
@ContextConfiguration(classes = {RedisConfig.class})
public class CacheTest {
    private Logger logger = LoggerFactory.getLogger(CacheTest.class);

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    @Autowired
    private UserService userService;


    /**
     * 测试单参数
     */
    @Test
    public void test1() {
        System.out.println("第1次查询id=1的数据：");
        User user = userService.getById(1);
        System.out.println(String.format("结果：{%s}", JSONObject.toJSONString(user)));
        System.out.println();
        System.out.println();
        System.out.println("第2次查询id=1的数据：");
        user = userService.getById(1);
        System.out.println(String.format("结果：{%s}", JSONObject.toJSONString(user)));
    }

    /**
     * 测试多参数
     */
    @Test
    public void test2() {
        System.out.println("第1次查询id=2的数据：");
        User user = userService.getByIdAndName(2,"fenganran");
        System.out.println(String.format("结果：{%s}", JSONObject.toJSONString(user)));
        System.out.println();
        System.out.println();
        System.out.println("第2次查询id=2的数据：");
        user = userService.getByIdAndName(2,"fenganran");
        System.out.println(String.format("结果：{%s}", JSONObject.toJSONString(user)));
    }

    /**
     * 测试对象参数
     */
    @Test
    public void test3() {
        System.out.println("第1次查询id=2的数据：");
        User user = new User();
        user.setId(3); user.setName("fengxiansheng");
        user = userService.getByObj(user);
        System.out.println(String.format("结果：{%s}", JSONObject.toJSONString(user)));
        System.out.println();
        System.out.println();
        System.out.println("第2次查询id=2的数据：");
        user = userService.getByObj(user);
        System.out.println(String.format("结果：{%s}", JSONObject.toJSONString(user)));
    }

    /**
     * 测试删除缓存
     */
    @Test
    public void test4() throws InterruptedException {
        System.out.println("第1次查询id=1的数据：");
        User user = userService.getById(1);
        System.out.println(String.format("结果：{%s}", JSONObject.toJSONString(user)));
        System.out.println();
        System.out.println();
        System.out.println("删除id=1的数据：");
        userService.deleteById(1);
        Thread.sleep(3000);
        System.out.println("第2次查询id=1的数据：");
        user = userService.getById(1);

    }

    @Test
    public void testRedisMsg() {
        redisTemplate.convertAndSend(Constants.CLEAR_CACHE_TOPIC,new CacheMessage("testfeng1",new CacheConfig()));
        redisTemplate.convertAndSend(Constants.DELETE_CACHE_TOPIC,new CacheMessage("testfeng2",new CacheConfig()));
    }

}

