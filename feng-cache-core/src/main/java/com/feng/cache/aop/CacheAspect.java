package com.feng.cache.aop;

import com.feng.cache.annotation.CacheEvict;
import com.feng.cache.annotation.Cacheable;
import com.feng.cache.config.CacheConfig;
import com.feng.cache.key.KeyGenerator;
import com.feng.cache.key.SimpleKeyGenerator;
import com.feng.cache.key.spel.CacheOperationExpressionEvaluator;
import com.feng.cache.manager.Cache;
import com.feng.cache.manager.CacheManager;
import com.feng.cache.parser.CacheAnnotationParser;
import com.feng.cache.support.CacheOperationInvoker;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.framework.AopProxyUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.BeanFactoryAnnotationUtils;
import org.springframework.context.expression.AnnotatedElementKey;
import org.springframework.core.BridgeMethodResolver;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.data.redis.serializer.SerializationException;
import org.springframework.expression.EvaluationContext;
import org.springframework.util.ClassUtils;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.lang.reflect.Method;
import java.util.Collection;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;

@Aspect
public class CacheAspect implements BeanFactoryAware {
    protected static final Logger logger = LoggerFactory.getLogger(CacheAspect.class);

    private CacheOperationExpressionEvaluator evaluator = new CacheOperationExpressionEvaluator();

    private BeanFactory beanFactory;

    private KeyGenerator defaultKeyGenerator = new SimpleKeyGenerator();
    @Autowired
    private CacheManager cacheManager;
    private CacheAnnotationParser cacheAnnotationParser = new CacheAnnotationParser();

    @Pointcut("@annotation(com.feng.cache.annotation.Cacheable)")
    public void cacheablePointcut() {
    }

    @Pointcut("@annotation(com.feng.cache.annotation.CacheEvict)")
    public void cacheEvictPointcut() {
    }

    @Around("cacheablePointcut()")
    public Object cacheablePointcut(ProceedingJoinPoint joinPoint) {
        CacheOperationInvoker cacheOperationInvoker = getCacheOperationInvoker(joinPoint);
        Cache cache = null;
        Object key = null;
        Method method = getTargetMethod(joinPoint);
        // 获取注解
        Cacheable cacheable = AnnotationUtils.findAnnotation(method, Cacheable.class);
        try{
            // 根据配置拿到对应的Cache
            CacheConfig cacheConfig = cacheAnnotationParser.parserCacheAnnotatoins(method);
            cache = cacheManager.getCache(cacheConfig);
            // key的生成
            key = generateKey(cacheable.key(),cacheable.keyGenerator(),method,joinPoint.getArgs(),joinPoint.getTarget());
            return cache.get(key,() -> cacheOperationInvoker.invoke());
        } catch (Exception e){
            logger.error("从缓存中获取数据失败:{}",e.getMessage());
            logger.error("从缓存中获取数据失败:",e);
            if(e instanceof SerializationException
                    && cache!=null){
                // 如果是序列化异常需要先删除原有缓存
                cache.evict(key);
            }
            // 忽略操作缓存过程中遇到的异常
            if (cacheable.ignoreException()) {
                return cacheOperationInvoker.invoke();
            }
            throw e;
        }
    }

    @Around("cacheEvictPointcut()")
    public Object cacheEvictPointcut(ProceedingJoinPoint joinPoint) {
        CacheOperationInvoker cacheOperationInvoker = getCacheOperationInvoker(joinPoint);
        Method method = getTargetMethod(joinPoint);
        // 获取注解
        CacheEvict cacheEvict = AnnotationUtils.findAnnotation(method, CacheEvict.class);
        try{
            Cache cache = null;
            if(StringUtils.hasText(cacheEvict.targetCache())){
                // 根据配置拿到对应的Cache
                CacheConfig cacheConfig = cacheAnnotationParser.parserCacheAnnotatoins(method);
                //解析@CacheEvict有可能返回的cacheConfig是null，因为在调用清除缓存的方法之前并没有调用过存储缓存的方法
                cache = cacheConfig == null ? null : cacheManager.getCache(cacheConfig);

            }
            Collection<Cache> caches = cacheManager.getCaches(cacheEvict.cacheName());
            // key的生成
            Object key = generateKey(cacheEvict.key(),cacheEvict.keyGenerator(),method,joinPoint.getArgs(),joinPoint.getTarget());
            if(cacheEvict.beforeInvocation()){
                if(StringUtils.hasText(cacheEvict.targetCache())){
                    deleteCache(cache,key,cacheEvict.allEntries());
                }else{
                    deleteCache(caches,key,cacheEvict.allEntries());
                }
            }
            Object value = cacheOperationInvoker.invoke();
            // 当前线程阻塞一定的时间
            LockSupport.parkNanos(this, TimeUnit.MILLISECONDS.toNanos(cacheEvict.waittime()));
            if(StringUtils.hasText(cacheEvict.targetCache())){
                deleteCache(cache,key,cacheEvict.allEntries());
            }else{
                deleteCache(caches,key,cacheEvict.allEntries());
            }
            return value;
        } catch (Exception e){
            logger.error("从缓存中删除数据失败:{}",e.getMessage());
            logger.error("从缓存中删除数据失败:",e);
            throw e;
        }
    }

    /**
     * 删除缓存
     */
    private void deleteCache(Collection<Cache> caches, Object key,boolean allEntries) {
        if(allEntries && !CollectionUtils.isEmpty(caches)){//清除cacheName对应的所有缓存
            for(Cache cache : caches){
                cache.clear();
            }
        }
        if(!CollectionUtils.isEmpty(caches)){
            for(Cache cache : caches){
                cache.evict(key);
            }
        }
    }
    /**
     * 删除缓存
     */
    private void deleteCache(Cache cache, Object key,boolean allEntries) {
        if(cache==null) return;
        if(allEntries && cache!=null)
            cache.clear();
        if(cache!=null){
            cache.evict(key);
        }
    }

    /**
     * 获取key
     * @param keySpel
     * @param keyGenerator
     * @param method
     * @param args
     * @param target
     * @return
     */
    private Object generateKey(String keySpel, String keyGenerator, Method method, Object[] args, Object target) {
        Object key;
        if (StringUtils.hasText(keySpel)) {
            Class<?> targetClass = getTargetClass(target);
            EvaluationContext evaluationContext = evaluator.createEvaluationContext(method, args, target,
                    targetClass, CacheOperationExpressionEvaluator.NO_RESULT,beanFactory);
            key = evaluator.key(keySpel, new AnnotatedElementKey(method, targetClass), evaluationContext);
        }else{
            key = StringUtils.hasText(keyGenerator) ?
                    getBean(keyGenerator, KeyGenerator.class).generate(target, method, args) :
                    this.defaultKeyGenerator.generate(target, method, args);
        }
        if (key == null)
            throw new IllegalArgumentException(String.format("生成的key空，请检查key={%s},keyGenerator={%s}：",keySpel,keyGenerator));
        return key;
    }

    protected <T> T getBean(String beanName, Class<T> expectedType) {
        return BeanFactoryAnnotationUtils.qualifiedBeanOfType(this.beanFactory, expectedType, beanName);
    }

    private CacheOperationInvoker getCacheOperationInvoker(ProceedingJoinPoint joinPoint){
       return  () -> {
           try {
               return joinPoint.proceed();
           } catch (Throwable ex) {
               throw new CacheOperationInvoker.ThrowableWrapper(ex);
           }
       };
    }

    private Method getTargetMethod(ProceedingJoinPoint pjp) {
        //获取目标方法
        Method method = ((MethodSignature) pjp.getSignature()).getMethod();
        //获取目标类
        Class<?> targetClass = getTargetClass(pjp.getTarget());
        //获得最匹配的一个可以执行的方法；比如传入IEmployeeService.someLogic方法，
        //在EmployeeServiceImpl类上找到匹配的EmployeeServiceImpl.someLogic方法，这个方法是一个可以执行的方法
        Method specificMethod = ClassUtils.getMostSpecificMethod(method, targetClass);
        //TODO 下面代码没搞懂
        return BridgeMethodResolver.findBridgedMethod(specificMethod);
    }

    private Class<?> getTargetClass(Object target) {
        //ultimateTargetClass 获取代理对象的最终对象类型
        Class<?> targetClass = AopProxyUtils.ultimateTargetClass(target);
        if (targetClass == null && target != null) {
            targetClass = target.getClass();
        }
        return targetClass;
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        this.beanFactory = beanFactory;
    }
}

