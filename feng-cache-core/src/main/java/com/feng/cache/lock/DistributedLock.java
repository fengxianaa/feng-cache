package com.feng.cache.lock;

import java.util.UUID;

/**
 * 分布式锁顶级接口
 */
public interface DistributedLock {

    /**
     * 锁的默认失效时间,单位：毫秒
     */
    long TIMEOUT_MILLIS = 30000;

    /**
     * 获取锁的重试次数，默认：0 不重试
     */
    int RETRY_TIMES = 0;

    /**
     * 重新获取锁的等待时间,单位：毫秒
     */
    long SLEEP_MILLIS = 500;

    /**
     * 是否重新刷新锁的过期时间
     */
    boolean FRESH_TIMEOUT=false;


    /**
     * 分布式锁，默认300秒失效
     * @param key
     * @return
     */
    default boolean lock(String key){
        String val = UUID.randomUUID().toString();
        this.setDefaultValue(val);
        return lock(key, val,TIMEOUT_MILLIS,RETRY_TIMES,SLEEP_MILLIS,FRESH_TIMEOUT);
    }

    void setDefaultValue(String val);

    /**
     * 分布式锁，默认300秒失效
     * @param key
     * @param value
     * @return
     */
    default boolean lock(String key, String value){
        return lock(key,value,TIMEOUT_MILLIS,RETRY_TIMES,SLEEP_MILLIS,FRESH_TIMEOUT);
    }

    /**
     * 分布式锁，默认300秒失效
     * @param key
     * @param value
     * @param retryTimes 重试次数
     * @return
     */
    default boolean lock(String key, String value, int retryTimes){
        return lock(key,value,TIMEOUT_MILLIS,retryTimes,SLEEP_MILLIS, FRESH_TIMEOUT);
    }

    /**
     * 分布式锁，默认300秒失效
     * @param key
     * @param value
     * @param retryTimes 重试次数
     * @param sleepMillis 下次重试的等待时间
     * @return
     */
    default boolean lock(String key, String value, int retryTimes, long sleepMillis){
        return lock(key,value,TIMEOUT_MILLIS,retryTimes,sleepMillis,FRESH_TIMEOUT);
    }

    /**
     * 分布式锁
     * @param key
     * @param value
     * @param expire 失效时间,毫秒
     * @param freshTimeout 如果锁存在且value相同，是否刷新过期时间，true：刷新新的过时间为expire
     * @return
     */
    default boolean lock(String key, String value, long expire, boolean freshTimeout){
        return lock(key,value,expire,RETRY_TIMES,SLEEP_MILLIS,freshTimeout);
    }

    /**
     * 分布式锁
     * @param key
     * @param value
     * @param expire 失效时间，毫秒
     * @param retryTimes 重试次数
     * @param freshTimeout 如果锁存在且value相同，是否刷新过期时间，true：刷新新的过时间为expire
     * @return
     */
    default boolean lock(String key, String value, long expire, int retryTimes, boolean freshTimeout){
        return lock(key,value,expire,retryTimes,SLEEP_MILLIS,freshTimeout);
    }

    /**
     * 分布式锁
     * @param key
     * @param value
     * @param expire 失效时间，毫秒
     * @param retryTimes 重试次数
     * @param sleepMillis 下次重试的等待时间
     * @param freshTimeout 如果锁存在且value相同，是否刷新过期时间，true：刷新新的过时间为expire
     * @return
     */
    boolean lock(String key, String value, long expire, int retryTimes, long sleepMillis, boolean freshTimeout);

    /**
     * 释放分布式锁
     * @param key
     * @param value
     * @return
     */
    boolean unLock(String key, String value);

    /**
     * 释放分布式锁,默认使用defaultValue解锁
     * @param key
     * @return
     */
    default boolean unLock(String key){
        return unLock(key,this.getDefaultValue());
    }

    String getDefaultValue();
}
