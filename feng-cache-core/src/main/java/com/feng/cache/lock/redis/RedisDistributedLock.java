package com.feng.cache.lock.redis;

import com.feng.cache.lock.DistributedLock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.script.RedisScript;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.JedisCommands;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * redis分布式锁
 */
public class RedisDistributedLock implements DistributedLock {

    private final Logger logger = LoggerFactory.getLogger(RedisDistributedLock.class);

    private RedisTemplate<String,Object> redisTemplate;

    private String defaultValue;

    /**
     * 解锁lua脚本
     */
    private static final String UNLOCK_LUA;

    /**
     * 申请锁并刷新失效时间lua脚本
     */
    private static final String LOCK_AND_FRESH_TIMEOUT_LUA;

    public RedisDistributedLock(RedisTemplate<String,Object> redisTemplate){
        this.redisTemplate = redisTemplate;
    }

    /*
        if redis.call('setNx',KEYS[1],ARGV[1])==1
            then return redis.call('expire',KEYS[1],ARGV[2])
        else
            if redis.call('get',KEYS[1])==ARGV[1]
                then return redis.call('expire',KEYS[1],ARGV[2])
            else
                return 0
            end
        end
     */
    static {
        StringBuilder sb = new StringBuilder();
        sb.append("if redis.call('get',KEYS[1]) == ARGV[1] ");
        sb.append("then ");
        sb.append("    return redis.call('del',KEYS[1]) ");
        sb.append("else ");
        sb.append("    return 0 ");
        sb.append("end ");
        UNLOCK_LUA = sb.toString();

        sb.delete(0,sb.length());

        sb.append("if redis.call('setNx',KEYS[1],ARGV[1])==1 ");
        sb.append("    then ");
        sb.append("        return redis.call('pexpire',KEYS[1],ARGV[2]) ");
        sb.append("else ");
        sb.append("    if redis.call('get',KEYS[1])==ARGV[1] ");
        sb.append("    then ");
        sb.append("        return redis.call('pexpire',KEYS[1],ARGV[2]) ");
        sb.append("    else ");
        sb.append("        return 0 ");
        sb.append("    end ");
        sb.append("end ");
        LOCK_AND_FRESH_TIMEOUT_LUA = sb.toString();
    }

    @Override
    public String getDefaultValue() {
        return defaultValue;
    }

    @Override
    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    /**
     * 分布式锁
     * @param key
     * @param value
     * @param expire 失效时间,毫秒
     * @param retryTimes 重试次数
     * @param sleepMillis 下次重试的等待时间
     * @param freshTimeout 如果锁存在且value相同，是否刷新过期时间，true：刷新新的过时间为expire
     * @return
     */
    @Override
    public boolean lock(String key, String value, long expire, int retryTimes, long sleepMillis, boolean freshTimeout) {
        boolean bool = getLock(key,value,expire,freshTimeout);
        while (!bool && (retryTimes-- > 0)){
            //开始重试获取锁
            try { Thread.sleep(sleepMillis); } catch (InterruptedException e) { return false; }
            bool = getLock(key,value,expire,freshTimeout);
        }
        logger.info("Redis锁 {}--{} 的申请结果：{}",key,value,bool);
        return bool;
    }

    private boolean getLock(String key, String value, long expire, boolean freshTimeout) {
        try{
            if(freshTimeout){
                //如果锁存在且value相同，刷新过期时间
                List<String> keys = Arrays.asList(key);
                List<String> args = Arrays.asList(value,String.valueOf(expire));
                return redisTemplate.execute((RedisCallback<Boolean>) connection -> execureLua(connection,LOCK_AND_FRESH_TIMEOUT_LUA,keys,args));
            }
            //TODO 优化加锁方式
            Boolean bool = redisTemplate.opsForValue().setIfAbsent(key, value, expire, TimeUnit.MILLISECONDS);
//            return redisTemplate.execute((RedisCallback<Boolean>) connection -> {
//                JedisCommands nativeConnection = (JedisCommands) connection.getNativeConnection();
//                String result = nativeConnection.set(key, value, "NX", "PX", expire);
//                return StringUtils.hasText(result);
//            });
            return bool!=null && bool;
        }catch (Exception e){
            logger.error("添加redis锁: {}-{} 失败",key,value);
            logger.error(e.getMessage(),e);
        }
        return false;
    }

    private boolean execureLua(RedisConnection connection, String lua, List<String> keys, List<String> args){
        Object nativeConnection = connection.getNativeConnection();
        //集群模式和单机模式虽然执行脚本的方法一样，但是没有公共的接口，只能分开执行
        if (nativeConnection instanceof JedisCluster) {
            //集群模式
            return 1l == (Long) ((JedisCluster) nativeConnection).eval(lua, keys, args);
        } else if (nativeConnection instanceof Jedis) {
            //单机模式
            return 1l == (Long) ((Jedis) nativeConnection).eval(lua, keys, args);
        }
        return false;

    }

    /**
     * 释放分布式锁
     * @param key
     * @param value
     * @return
     */
    @Override
    public boolean unLock(String key, String value) {
        try{
            List<String> keys = Arrays.asList(key);
            List<String> args = Arrays.asList(value);
            RedisScript<Long> script = RedisScript.of(UNLOCK_LUA, Long.class);
            Long res = redisTemplate.execute(script, keys, value);
            return res==1;
            //TODO
//            return redisTemplate.execute((RedisCallback<Boolean>) connection -> execureLua(connection,UNLOCK_LUA,keys,args));
        }catch (Exception e){
            logger.error("释放redis锁: {}-{} 失败",key,value);
            logger.error(e.getMessage(),e);
        }
        return false;
    }
}
