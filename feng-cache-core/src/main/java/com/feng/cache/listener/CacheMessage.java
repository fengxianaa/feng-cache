package com.feng.cache.listener;

import com.feng.cache.config.CacheConfig;

import java.io.Serializable;

public class CacheMessage implements Serializable {

    private Object key;
    private CacheConfig cacheConfig;
    //是否清空整个缓存
    private boolean clear;

    public CacheMessage(){}

    public CacheMessage(Object key, CacheConfig cacheConfig){
        this.key = key;
        this.cacheConfig = cacheConfig;
    }

    public Object getKey() {
        return key;
    }

    public void setKey(Object key) {
        this.key = key;
    }

    public CacheConfig getCacheConfig() {
        return cacheConfig;
    }

    public void setCacheConfig(CacheConfig cacheConfig) {
        this.cacheConfig = cacheConfig;
    }

    public boolean isClear() {
        return clear;
    }

    public void setClear(boolean clear) {
        this.clear = clear;
    }
}
