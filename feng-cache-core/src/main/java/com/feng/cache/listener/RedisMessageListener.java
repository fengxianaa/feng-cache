package com.feng.cache.listener;

import com.alibaba.fastjson.JSONObject;
import com.feng.cache.config.MultiCacheConfig;
import com.feng.cache.manager.CacheManager;
import com.feng.cache.manager.MultiCacheManager;
import com.feng.cache.manager.caffeine.CaffeineCache;
import com.feng.cache.manager.multi.MultiCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;

import java.nio.charset.Charset;

public class RedisMessageListener implements MessageListener {
    protected static final Logger logger = LoggerFactory.getLogger(RedisMessageListener.class);

    private CacheManager cacheManager;

    @Override
    public void onMessage(Message message, byte[] pattern) {
        logger.info("redis消息监听器拿到消息:{}",new String(message.getBody(), Charset.forName("UTF8")));
        try {
            MultiCacheManager manager = (MultiCacheManager) cacheManager;
            //从cacheManager中取出redisTemplte解析消息
            CacheMessage cacheMessage = (CacheMessage) manager.getRedisTemplate().getValueSerializer().deserialize(message.getBody());
            //获取CaffeineCache
            CaffeineCache cache = ((MultiCache)manager.getCache(cacheMessage.getCacheConfig())).getCaffeineCache();
            if(cacheMessage.isClear())//清空缓存
                cache.clear();
            else
                cache.evict(cacheMessage.getKey());
        } catch (Exception e) {
            logger.error("接收redis消息清除CaffeineCache缓存失败："+e.getMessage(),e);
        }
    }

    public CacheManager getCacheManager() {
        return cacheManager;
    }

    public void setCacheManager(CacheManager cacheManager) {
        this.cacheManager = cacheManager;
    }

}
