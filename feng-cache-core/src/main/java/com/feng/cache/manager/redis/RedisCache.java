package com.feng.cache.manager.redis;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.feng.cache.config.CacheConfig;
import com.feng.cache.config.single.RedisCacheConfig;
import com.feng.cache.lock.DistributedLock;
import com.feng.cache.lock.redis.RedisDistributedLock;
import com.feng.cache.manager.AbstractValueAdaptingCache;
import com.feng.cache.support.NullValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import java.util.Comparator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;

/**
 * 基于Redis实现的二级缓存
 */
public class RedisCache extends AbstractValueAdaptingCache {
    protected static final Logger logger = LoggerFactory.getLogger(RedisCache.class);

    /**
     * 刷新缓存重试次数
     */
    private static final int RETRY_COUNT = 3;

    /**
     * 刷新缓存等待时间，单位毫秒
     */
    private static final long WAIT_TIME = 20;

    /**
     * 等待线程容器
     */
    private final Map<String, Set<Thread>> threadWaitMap = new ConcurrentHashMap<>();

    /**
     * redis 客户端
     */
    private RedisTemplate<String, Object> redisTemplate;

    //redis缓存配置
    private RedisCacheConfig config;

    /**
     * @param redisTemplate         redis客户端 redis 客户端
     * @param redisCacheConfig      二级缓存配置{@link RedisCacheConfig}
     */
    public RedisCache(RedisTemplate<String, Object> redisTemplate, RedisCacheConfig redisCacheConfig) {
        super(redisCacheConfig.getCacheName());
        Assert.notNull(redisTemplate, "RedisTemplate 不能为NULL");
        this.redisTemplate = redisTemplate;
        this.config = redisCacheConfig;
    }

    @Override
    public RedisTemplate<String, Object> getNativeCache() {
        return this.redisTemplate;
    }

    @Override
    public Object get(Object key) {
//        if (isStats()) {
//            getCacheStats().addCacheRequestCount(1);
//        }

        String redisCacheKey = getRedisCacheKey(key);
        logger.debug("redis缓存 key={} 查询redis缓存", redisCacheKey);
        return fromCacheValue(redisTemplate.opsForValue().get(redisCacheKey));
    }

    @Override
    public <T> T get(Object key, Callable<T> valueLoader) {
//        if (isStats()) {
//            getCacheStats().addCacheRequestCount(1);
//        }

        String redisCacheKey = getRedisCacheKey(key);
        logger.debug("redis缓存 key={} 查询redis缓存如果没有命中，从目标方法获取数据", redisCacheKey);
        // 先获取缓存，如果有直接返回
        Object result = redisTemplate.opsForValue().get(redisCacheKey);
        if (result != null) {
            // 刷新缓存
            //refreshCache(redisCacheKey, valueLoader, result);
            return (T) fromCacheValue(result);
        }
        // 执行目标方法(查询数据库)
        return invokeMethod(redisCacheKey, valueLoader);
    }

    @Override
    public void put(Object key, Object value) {
        String redisCacheKey = getRedisCacheKey(key);
        logger.debug("redis缓存 key={} put缓存，缓存值：{}", redisCacheKey, JSON.toJSONString(value));
        putValue(redisCacheKey, value);
    }

    @Override
    public Object putIfAbsent(Object key, Object value) {
        logger.debug("redis缓存 key={} putIfAbsent缓存，缓存值：{}", key, JSON.toJSONString(value));
        Object reult = get(key);
        if (reult != null) {
            return reult;
        }
        put(key, value);
        return null;
    }

    @Override
    public void evict(Object key) {
        String redisCacheKey = getRedisCacheKey(key);
        logger.info("清除redis缓存 key={} ", redisCacheKey);
        redisTemplate.delete(redisCacheKey);
    }

    @Override
    public void clear() {
        // 必须开启了使用缓存名称作为前缀，clear才有效
        if (config.isUsePrefix()) {
            logger.info("清空redis缓存 ，缓存前缀为{}", getName());
            Set<String> keys = redisTemplate.keys(getName() + "*");
            if (!CollectionUtils.isEmpty(keys)) {
                redisTemplate.delete(keys);
            }
        }
    }

    @Override
    public CacheConfig getCacheConfig() {
        return this.config;
    }


    /**
     * 获取redis中真正存储的key(有可能以cacheName为前缀)
     *
     * @param key 缓存key
     * @return RedisCacheKey
     */
    public String getRedisCacheKey(Object key) {
        return new RedisCacheKey(key, redisTemplate.getKeySerializer())
                .cacheName(getName()).usePrefix(config.isUsePrefix()).getKey();
    }

    /**
     * 执行目标方法获取数据，并放到redis缓存中
     */
    private <T> T invokeMethod(String redisCacheKey, Callable<T> valueLoader) {
        // 先取缓存，如果有直接返回，没有再去做拿锁操作
        Object result = redisTemplate.opsForValue().get(redisCacheKey);
        if (result != null) {
            logger.debug("redis缓存 key={} 在抢占锁之前查询缓存,拿到数据直接返回", redisCacheKey);
            return (T) fromCacheValue(result);
        }
        logger.info("redis缓存 key={} ，开始竞争锁，从目标方法加载数据到缓存",redisCacheKey);
        DistributedLock lock = new RedisDistributedLock(redisTemplate);
        String lockKey = redisCacheKey + "_sync_lock";
        int i =0;
        do{
            try{
                if(lock.lock(lockKey)){
                    //拿到锁，去数据库查询并把结果放入缓存
                    T t = loaderAndPutValue(redisCacheKey, valueLoader);
                    logger.debug("redis缓存 key={} 从数据库获取数据完毕，唤醒等待线程", redisCacheKey);
                    // 唤醒等待的线程
                    signalAll(redisCacheKey);
                    return t;
                }
                for(int j =0;j<RETRY_COUNT;j++){
                    logger.debug("redis缓存 key={} 没有拿到锁，进入等待状态，每 {} 毫秒，查redis缓存一次", redisCacheKey, WAIT_TIME);
                    await(redisCacheKey,WAIT_TIME);
                    result = redisTemplate.opsForValue().get(redisCacheKey);
                    if (result != null) {
                        return (T) fromCacheValue(result);
                    }
                }
                i++;
            } catch (Exception e) {
                throw new LoaderCacheValueException(redisCacheKey, e);
            } finally {
                lock.unLock(lockKey);
            }
        }while (i<RETRY_COUNT);
        logger.debug("redis缓存 key={} 等待{}次，共{}毫秒，任未获取到缓存，直接去执行目标方法", redisCacheKey, RETRY_COUNT, RETRY_COUNT * WAIT_TIME);
        return loaderAndPutValue(redisCacheKey, valueLoader);
    }

    /**
     * 加载并将数据放到redis缓存
     */
    private <T> T loaderAndPutValue(String key, Callable<T> valueLoader) {
        long start = System.currentTimeMillis();
        try {
            // 加载数据
            Object result = putValue(key, valueLoader.call());
            logger.debug("redis缓存 key={} 执行目标方法，更新缓存, 耗时：{}。数据:{}", key, System.currentTimeMillis() - start, JSONObject.toJSONString(result));
            return (T) fromCacheValue(result);
        } catch (Exception e) {
            throw new LoaderCacheValueException(key, e);
        }
    }

    private Object putValue(String key, Object value) {
        Object val = toCacheValue(value);
        // redis 缓存不允许直接存NULL，如果结果返回NULL需要删除缓存
        if (val == null
            || (!allowNullValue() && val instanceof NullValue)) {
            redisTemplate.delete(key);
            return val;
        }
        long expirationTime = config.getExpireTime();
        if (allowNullValue() && val instanceof NullValue) {
        // 如果允许缓存NULL值 且 值为null时 需要重新计算缓存时间
            expirationTime = expirationTime / config.getMagnification();
        }
        // 将数据放到缓存
        redisTemplate.opsForValue().set(key, val, expirationTime, config.getTimeUnit());
//        long expire = expirationTime;
//        redisTemplate.execute((RedisCallback<Boolean>) connection -> {
//            JedisCommands nativeConnection = (JedisCommands) connection.getNativeConnection();
//            String result = nativeConnection.set(key, val, "NX", "PX", TimeoutUtils.toMillis(expire,redisCacheConfig.getTimeUnit()));
//            return StringUtils.hasText(result);
//        });
        return val;
    }

    /**
     * 刷新缓存数据
     */
//    private <T> void refreshCache(RedisCacheKey redisCacheKey, Callable<T> valueLoader, Object result) {
//        Long ttl = redisTemplate.getExpire(redisCacheKey.getKey());
//        Long preload = preloadTime;
//        // 允许缓存NULL值，则自动刷新时间也要除以倍数
//        boolean flag = isAllowNullValues() && (result instanceof NullValue || result == null);
//        if (flag) {
//            preload = preload / getMagnification();
//        }
//        if (null != ttl && ttl > 0 && TimeUnit.SECONDS.toMillis(ttl) <= preload) {
//            // 判断是否需要强制刷新在开启刷新线程
//            if (!getForceRefresh()) {
//                logger.debug("redis缓存 key={} 软刷新缓存模式", redisCacheKey.getKey());
//                softRefresh(redisCacheKey);
//            } else {
//                logger.debug("redis缓存 key={} 强刷新缓存模式", redisCacheKey.getKey());
//                forceRefresh(redisCacheKey, valueLoader);
//            }
//        }
//    }

    /**
     * 软刷新，直接修改缓存时间
     *
     * @param redisCacheKey {@link RedisCacheKey}
     */
//    private void softRefresh(RedisCacheKey redisCacheKey) {
//        // 加一个分布式锁，只放一个请求去刷新缓存
//        Lock redisLock = new Lock(redisTemplate, redisCacheKey.getKey() + "_lock");
//        try {
//            if (redisLock.tryLock()) {
//                redisTemplate.expire(redisCacheKey.getKey(), this.expiration, TimeUnit.MILLISECONDS);
//            }
//        } catch (Exception e) {
//            logger.error(e.getMessage(), e);
//        } finally {
//            redisLock.unlock();
//        }
//    }

    /**
     * 硬刷新（执行被缓存的方法）
     *
     * @param redisCacheKey {@link RedisCacheKey}
     * @param valueLoader   数据加载器
     */
//    private <T> void forceRefresh(RedisCacheKey redisCacheKey, Callable<T> valueLoader) {
//        // 尽量少的去开启线程，因为线程池是有限的
//        ThreadTaskUtils.run(() -> {
//            // 加一个分布式锁，只放一个请求去刷新缓存
//            Lock redisLock = new Lock(redisTemplate, redisCacheKey.getKey() + "_lock");
//            try {
//                if (redisLock.lock()) {
//                    // 获取锁之后再判断一下过期时间，看是否需要加载数据
//                    Long ttl = redisTemplate.getExpire(redisCacheKey.getKey());
//                    if (null != ttl && ttl > 0 && TimeUnit.SECONDS.toMillis(ttl) <= preloadTime) {
//                        // 加载数据并放到缓存
//                        loaderAndPutValue(redisCacheKey, valueLoader, false);
//                    }
//                }
//            } catch (Exception e) {
//                logger.error(e.getMessage(), e);
//            } finally {
//                redisLock.unlock();
//            }
//        });
//    }

    /**
     * 是否强制刷新（执行被缓存的方法），默认是false
     *
     * @return boolean
     */
//    private boolean getForceRefresh() {
//        return forceRefresh;
//    }

    @Override
    public boolean allowNullValue() {
        return config.isAllowNullValue();
    }


    /**
     * 把key相同的线程放入同一个容器，方便之后统一唤醒
     * @param key redis缓存Key
     * @param milliseconds 等待时间
     */
    private final void await(String key, long milliseconds) {
        Set<Thread> threadSet = threadWaitMap.get(key);
        // 判断线程容器是否是null，如果是就新创建一个
        if (threadSet == null) {
            threadSet = new ConcurrentSkipListSet<>(Comparator.comparing(Thread::toString));
            threadWaitMap.put(key, threadSet);
//            synchronized (key){
//                threadSet = threadMap.get(key);
//                if(threadSet==null){
//                    threadSet = new ConcurrentSkipListSet<>(Comparator.comparing(Thread::toString));
//                }
//                threadMap.put(key, threadSet);
//            }
        }
        threadSet.add(Thread.currentThread());
        // 当前线程阻塞一定的时间
        LockSupport.parkNanos(this, TimeUnit.MILLISECONDS.toNanos(milliseconds));
    }

    /**
     * 唤醒key对应的等待线程
     * @param key key
     */
    private final void signalAll(String key) {
        Set<Thread> threadSet = threadWaitMap.get(key);
        // 判断key所对应的等待线程容器是否是null
        if (!CollectionUtils.isEmpty(threadSet)) {
            for (Thread thread : threadSet) {
                LockSupport.unpark(thread);
            }
            // 清空等待线程容器
            threadSet.clear();
            threadWaitMap.remove(key);
        }
    }
}
