package com.feng.cache.manager.caffeine;

import com.alibaba.fastjson.JSON;
import com.feng.cache.config.CacheConfig;
import com.feng.cache.config.single.CaffeineCacheConfig;
import com.feng.cache.manager.AbstractValueAdaptingCache;
import com.feng.cache.support.ExpireMode;
import com.feng.cache.support.NullValue;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Callable;

/**
 * 基于Caffeine实现的一级缓存(本地缓存)
 */
public class CaffeineCache extends AbstractValueAdaptingCache {
    protected static final Logger logger = LoggerFactory.getLogger(CaffeineCache.class);

    //Caffeine配置
    private CaffeineCacheConfig config;

    /**
     * 缓存对象
     */
    private Cache<Object, Object> cache;

    /**
     * 通过构造方法设置缓存配置
     *
     * @param config Caffeine配置
     */
    public CaffeineCache(CaffeineCacheConfig config) {
        super(config.getCacheName());
        this.config = config;
        buildCache();
    }

    @Override
    public Object getNativeCache() {
        return this.cache;
    }

    @Override
    public Object get(Object key) {
        Object val = this.cache.getIfPresent(key);
        logger.debug("从caffeine缓存取数据 {}={}", JSON.toJSONString(key),JSON.toJSONString(val));
        return fromCacheValue(val);
    }

    @Override
    public <T> T get(Object key, Callable<T> valueLoader) {
        logger.debug("从caffeine缓存取数据 key={}", JSON.toJSONString(key));
        Object result = this.cache.get(key,k -> loaderValue(k,valueLoader));
        //如果不允许存储空值，删除对应的缓存
        if(!allowNullValue()
            && (result==null || result instanceof NullValue)){
            evict(key);
        }
        return (T) fromCacheValue(result);
    }

    private <T> Object loaderValue(Object key,Callable<T> valueLoader) {
        try {
            T value = valueLoader.call();
            return toCacheValue(value);
        } catch (Exception e) {
            throw new LoaderCacheValueException(key, e);
        }
    }

    @Override
    public void put(Object key, Object value) {
        // 如果允许存NULL值，或者value不是null
        if (allowNullValue() || (value != null && value instanceof NullValue)) {
            logger.debug("往caffeine中添加缓存 {}={} ", JSON.toJSONString(key), JSON.toJSONString(value));
            this.cache.put(key, toCacheValue(value));
            return;
        }
        logger.debug("缓存值为NULL并且不允许存NULL值，不缓存数据");
    }

    /**
     * 如果缓存key没有对应的值就将值put到缓存，如果有就直接返回原有的值
     * <p>就相当于:
     * <pre><code>
     * Object existingValue = cache.get(key);
     * if (existingValue == null) {
     *     cache.put(key, value);
     *     return null;
     * } else {
     *     return existingValue;
     * }
     * </code></pre>
     * @param key   缓存key
     * @param value 缓存key对应的值
     * @return 因为值本身可能为NULL，或者缓存key本来就没有对应值的时候也为NULL，
     * 所以如果返回NULL就表示已经将key-value键值对放到了缓存中
     * @since 4.1
     */
    @Override
    public Object putIfAbsent(Object key, Object value) {
        logger.debug("往caffeine中添加缓存 {}={} ", JSON.toJSONString(key), JSON.toJSONString(value));
        //先判断是否允许存空值
        if(!allowNullValue() && (value==null || value instanceof NullValue)) return null;
        Object result = this.cache.get(key,k -> toCacheValue(value));
        return fromCacheValue(result);
    }

    @Override
    public void evict(Object key) {
        logger.debug("清除caffeine缓存，key={} ", JSON.toJSONString(key));
        this.cache.invalidate(key);
    }

    @Override
    public void clear() {
        logger.debug("清空caffeine缓存");
        this.cache.invalidateAll();
    }

    @Override
    public CacheConfig getCacheConfig() {
        return this.config;
    }

    /**
     * Caffeine默认不允许存储空值
     * @return
     */
    @Override
    public boolean allowNullValue() {
        return config.isAllowNullValue();
    }

    /**
     * 根据配置创建Caffeine Cache对象
     */
    private void buildCache() {
        Caffeine<Object, Object> builder = Caffeine.newBuilder();
        builder.initialCapacity(config.getInitialCapacity());
        builder.maximumSize(config.getMaximumSize());
        if (ExpireMode.WRITE.equals(config.getExpireMode())) {
            builder.expireAfterWrite(config.getExpireTime(), config.getTimeUnit());
        } else if (ExpireMode.ACCESS.equals(config.getExpireMode())) {
            builder.expireAfterAccess(config.getExpireTime(), config.getTimeUnit());
        }
        this.cache = builder.build();
    }
}
