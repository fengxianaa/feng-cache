package com.feng.cache.manager;

import com.feng.cache.config.CacheConfig;
import com.feng.cache.config.MultiCacheConfig;
import com.feng.cache.manager.multi.MultiCache;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class MultiCacheManager implements CacheManager, InitializingBean {
    private final ConcurrentMap<String, Map<String, Cache>> cacheMap = new ConcurrentHashMap<>(16);

    private volatile Set<String> cacheNames = Collections.emptySet();

    private RedisTemplate<String, Object> redisTemplate;

//    @Override
//    public Collection<Cache> getCache(String cacheName) {
//        Map<String, Cache> caches = this.cacheMap.get(cacheName);
//        if (caches != null) {
//            return caches.values();
//        }
//        return null;
//    }

    public MultiCacheManager(RedisTemplate<String, Object> redisTemplate){
        this.redisTemplate = redisTemplate;
    }

    @Override
    public Collection<Cache> getCaches(String cacheName) {
        Map<String, Cache> caches = cacheMap.get(cacheName);
        if(CollectionUtils.isEmpty(caches)) return Collections.emptyList();
        return caches.values();
    }

    @Override
    public Cache getCache(CacheConfig cacheConfig) {
        Cache cache = null;
        Map<String,Cache> caches = this.cacheMap.get(cacheConfig.getCacheName());
        if (!CollectionUtils.isEmpty(caches)) {
            cache = caches.get(cacheConfig.toString());
        }
        if(cache!=null) return cache;

        synchronized (this.cacheMap) {
            caches = this.cacheMap.get(cacheConfig.getCacheName());
            if (caches == null) {
                caches = new ConcurrentHashMap<>();
                caches.put(cacheConfig.toString(),getMissingCache(cacheConfig));
                this.cacheMap.put(cacheConfig.getCacheName(), caches);
                updateCacheNames(cacheConfig.getCacheName());
            }
            cache = caches.get(cacheConfig.toString());
        }
        return cache;
    }


    @Override
    public Collection<String> getCacheNames() {
        return this.cacheNames;
    }


    @Override
    public void afterPropertiesSet() {
        initializeCaches();
    }

    /**
     * Initialize the static configuration of caches.
     * <p>Triggered on startup through {@link #afterPropertiesSet()};
     * can also be called to re-initialize at runtime.
     * @since 4.2.2
     * @see #loadCaches()
     */
    public void initializeCaches() {
//        Collection<? extends Cache> caches = loadCaches();
//
//        synchronized (this.cacheMap) {
//            this.cacheNames = Collections.emptySet();
//            this.cacheMap.clear();
//            Set<String> cacheNames = new LinkedHashSet<String>(caches.size());
//            for (Cache cache : caches) {
//                String name = cache.getName();
//                this.cacheMap.put(name, decorateCache(cache));
//                cacheNames.add(name);
//            }
//            this.cacheNames = Collections.unmodifiableSet(cacheNames);
//        }
    }

    /**
     * Load the initial caches for this cache manager.
     * <p>Called by {@link #afterPropertiesSet()} on startup.
     * The returned collection may be empty but must not be {@code null}.
     */
    protected Collection<? extends Cache> loadCaches(){
        return null;
    }

    /**
     * Update the exposed {@link #cacheNames} set with the given name.
     * <p>This will always be called within a full {@link #cacheMap} lock
     * and effectively behaves like a {@code CopyOnWriteArraySet} with
     * preserved order but exposed as an unmodifiable reference.
     * @param name the name of the cache to be added
     */
    private void updateCacheNames(String name) {
        Set<String> cacheNames = new LinkedHashSet<String>(this.cacheNames.size() + 1);
        cacheNames.addAll(this.cacheNames);
        cacheNames.add(name);
        this.cacheNames = Collections.unmodifiableSet(cacheNames);
    }

    private Cache getMissingCache(CacheConfig cacheConfig) {
        MultiCacheConfig config = (MultiCacheConfig) cacheConfig;
        return new MultiCache(this.redisTemplate,config);
    }

    public RedisTemplate<String,Object> getRedisTemplate(){
        return this.redisTemplate;
    }
}
