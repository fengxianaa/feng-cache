package com.feng.cache.manager;

import com.feng.cache.config.CacheConfig;

import java.util.Collection;

/**
 * 缓存管理器
 */
public interface CacheManager {

    /**
     * 根据cacheName拿到一组cache对象
     * @param cacheName
     * @return
     */
    Collection<Cache> getCaches(String cacheName);

    /**
     * 根据cacheConfig拿到对应的cache对象
     * @return
     */
    Cache getCache(CacheConfig cacheConfig);
    /**
     * Return a collection of the cache names known by this manager.
     * @return the names of all caches known by the cache manager
     */
    Collection<String> getCacheNames();

}
