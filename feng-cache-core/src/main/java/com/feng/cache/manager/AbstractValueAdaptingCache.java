/*
 * Copyright 2002-2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.feng.cache.manager;

import com.feng.cache.support.NullValue;
import org.springframework.util.Assert;


/**
 * Cache 接口的抽象实现类，对公共的方法做了一写实现，如是否允许存NULL值
 * <p>如果允许为NULL值，则需要在内部将NULL替换成{@link NullValue#INSTANCE} 对象
 *  *
 * @author yuhao.wang3
 */
public abstract class AbstractValueAdaptingCache implements Cache {

    /**
     * 缓存名称
     */
    private final String name;

    /**
     * 是否开启统计功能
     */
//    private boolean stats;

    /**
     * 缓存统计类
     */
//    private CacheStats cacheStats = new CacheStats();

    /**
     * 通过构造方法设置缓存配置
     *
     * @param name            缓存名称
     */
    protected AbstractValueAdaptingCache(String name) {
        Assert.notNull(name, "缓存名称不能为NULL");
        this.name = name;
    }

    /**
     * 获取是否允许存NULL值
     *
     * @return true:允许，false:不允许
     */
    public abstract boolean allowNullValue();

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T get(Object key, Class<T> type) {
        return (T) fromCacheValue(get(key));
    }

    /**
     * 把从缓存拿出来的值转换为用户需要的类型，主要是应对值为null这种情况
     *
     * @param cacheValue the store value
     * @return the value to return to the user
     */
    protected Object fromCacheValue(Object cacheValue) {
        if (allowNullValue() && cacheValue instanceof NullValue) {
            return null;
        }
        return cacheValue;
    }

    /**
     * 把从用户使用的值转换为缓存可以存储类型，主要是应对值为null这种情况
     *
     * @param userValue the given user value
     * @return the value to store
     */
    protected Object toCacheValue(Object userValue) {
        if (allowNullValue() && userValue == null) {
            return NullValue.INSTANCE;
        }
        return userValue;
    }
}
