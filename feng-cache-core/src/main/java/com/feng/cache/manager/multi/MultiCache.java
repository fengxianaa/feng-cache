package com.feng.cache.manager.multi;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.feng.cache.config.CacheConfig;
import com.feng.cache.config.MultiCacheConfig;
import com.feng.cache.listener.CacheMessage;
import com.feng.cache.manager.AbstractValueAdaptingCache;
import com.feng.cache.manager.caffeine.CaffeineCache;
import com.feng.cache.manager.redis.RedisCache;
import com.feng.cache.support.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.concurrent.Callable;

/**
 * 多级缓存(caffeine+redis)
 */
public class MultiCache extends AbstractValueAdaptingCache {
    protected static final Logger logger = LoggerFactory.getLogger(MultiCache.class);

    private MultiCacheConfig config;

    private AbstractValueAdaptingCache caffeineCache;

    private AbstractValueAdaptingCache redisCache;

    /**
     * 通过构造方法设置缓存配置
     *
     * @param config Caffeine配置
     */
    public MultiCache(RedisTemplate<String, Object> redisTemplate, MultiCacheConfig config) {
        super(config.getCacheName());
        this.config = config;
        buildCache(redisTemplate);
    }

    @Override
    public Object getNativeCache() {
        return this;
    }

    @Override
    public Object get(Object key) {
       return getOrLoader(key,null);
    }

    /**
     * 根据key从缓存中拿值，拿不到，就用valueLoader去数据库加载
     * @param key
     * @param valueLoader
     * @param <T>
     * @return
     */
    private <T> T getOrLoader(Object key,Callable<T> valueLoader){
        Object val = null;
        if(useCaffeineCache()){
            logger.debug("查本地缓存 key={}", JSON.toJSONString(key));
            val = this.caffeineCache.get(key);
        }
        if(val==null){
            logger.debug("查redis缓存 key={}", JSON.toJSONString(key));
            if(valueLoader!=null) val = this.redisCache.get(key,valueLoader);
            else val = this.redisCache.get(key);
            logger.debug("把从redis中拿到的结果放到caffeine {}={}", JSON.toJSONString(key),JSON.toJSONString(val));
            this.caffeineCache.putIfAbsent(key,val);
        }
        return (T) fromCacheValue(val);
    }

    @Override
    public <T> T get(Object key, Callable<T> valueLoader) {
        return getOrLoader(key,valueLoader);
    }

    @Override
    public void put(Object key, Object value) {
        this.redisCache.put(key,value);
        if(useCaffeineCache()) deleteCaffeineCacheBykey(key);
    }

    /**
     * 如果缓存key没有对应的值就将值put到缓存，如果有就直接返回原有的值
     * <p>就相当于:
     * <pre><code>
     * Object existingValue = cache.get(key);
     * if (existingValue == null) {
     *     cache.put(key, value);
     *     return null;
     * } else {
     *     return existingValue;
     * }
     * </code></pre>
     * @param key   缓存key
     * @param value 缓存key对应的值
     * @return 因为值本身可能为NULL，或者缓存key本来就没有对应值的时候也为NULL，
     * 所以如果返回NULL就表示已经将key-value键值对放到了缓存中
     * @since 4.1
     */
    @Override
    public Object putIfAbsent(Object key, Object value) {
        this.redisCache.putIfAbsent(key,value);
        if(useCaffeineCache()) deleteCaffeineCacheBykey(key);
        return value;
    }

    @Override
    public void evict(Object key) {
        this.redisCache.evict(key);
        if(useCaffeineCache()) deleteCaffeineCacheBykey(key);
    }

    @Override
    public void clear() {
        this.redisCache.clear();
        if(useCaffeineCache()) clearCaffeineCache();
    }

    @Override
    public CacheConfig getCacheConfig() {
        return this.config;
    }

    /**
     * 删除集群环境下的本地缓存(CaffeineCache)
     * @param key
     */
    private void deleteCaffeineCacheBykey(Object key) {
        logger.info("发送消息，删除集群环境下的本地缓存(CaffeineCache)，key={}", JSONObject.toJSONString(key));
        RedisTemplate<String, Object> redisTemplate = (RedisTemplate<String, Object>) this.redisCache.getNativeCache();
        redisTemplate.convertAndSend(Constants.DELETE_CACHE_TOPIC,new CacheMessage(key,this.config));
    }

    /**
     * 清空集群环境下的本地缓存(CaffeineCache)
     */
    private void clearCaffeineCache() {
        logger.info("发送消息，清空集群环境下的本地缓存(CaffeineCache)");
        RedisTemplate<String, Object> redisTemplate = (RedisTemplate<String, Object>) this.redisCache.getNativeCache();
        CacheMessage cacheMessage = new CacheMessage();
        cacheMessage.setClear(true);
        cacheMessage.setCacheConfig(this.config);
        redisTemplate.convertAndSend(Constants.DELETE_CACHE_TOPIC,cacheMessage);
    }

    /**
     * 默认不允许存储空值
     * @return
     */
    @Override
    public boolean allowNullValue() {
        return this.redisCache.allowNullValue() || this.caffeineCache.allowNullValue();
    }

    /**
     * 是否使用CaffeineCache
     * @return
     */
    public boolean useCaffeineCache(){ return this.config.isUseCaffeineCache(); }

    /**
     * 根据配置创建Caffeine和Redis Cache对象
     */
    private void buildCache(RedisTemplate<String, Object> redisTemplate) {
        if(useCaffeineCache())
            caffeineCache = new CaffeineCache(config.getCaffeineCacheConfig());
        redisCache = new RedisCache(redisTemplate, config.getRedisCacheConfig());
    }

    public CaffeineCache getCaffeineCache(){
        return (CaffeineCache)this.caffeineCache;
    }
}
