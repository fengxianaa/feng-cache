package com.feng.cache.annotation;

import java.lang.annotation.*;
import java.util.concurrent.TimeUnit;

/**
 * RedisCache缓存配置项
 *
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface RedisCache {
    /**
     * 缓存有效时间
     *
     * @return long
     */
    long expireTime() default 1;

    /**
     * 时间单位 {@link TimeUnit}
     *
     * @return TimeUnit
     */
    TimeUnit timeUnit() default TimeUnit.HOURS;

    /**
     * 是否允许缓存NULL值
     *
     * @return boolean
     */
    boolean allowNullValue() default false;

    /**
     * 是否使用缓存名称作为 redis key 前缀
     * 默认true,方便在redis界面化客户端查看缓存数据
     * @return boolean
     */
    boolean usePrefix() default true;

    /**
     * 非空值和null值之间的时间倍率，默认是10。isAllowNullValue=true才有效
     * <p>
     * 如配置缓存的有效时间是200秒，倍率这设置成10，
     * 那么当缓存value为null时，缓存的有效时间将是20秒，非空时为200秒
     * </p>
     *
     * @return int
     */
    int magnification() default 10;
}
