package com.feng.cache.annotation;


import com.feng.cache.support.ExpireMode;

import java.lang.annotation.*;
import java.util.concurrent.TimeUnit;

/**
 * CaffeineCache缓存配置项
 *
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface CaffeineCache {
    /**
     * 缓存初始Size
     *
     * @return int
     */
    int initialCapacity() default 50;

    /**
     * 缓存最大Size
     *
     * @return int
     */
    int maximumSize() default 1000;

    /**
     * 缓存有效时间
     *
     * @return int
     */
    int expireTime() default 10;

    /**
     * 缓存时间单位
     *
     * @return TimeUnit
     */
    TimeUnit timeUnit() default TimeUnit.MINUTES;

    /**
     * 缓存失效模式
     *
     * @return ExpireMode
     * @see ExpireMode
     */
    ExpireMode expireMode() default ExpireMode.WRITE;

    /**
     * 是否允许缓存NULL值
     *
     * @return boolean
     */
    boolean allowNullValue() default false;

}
