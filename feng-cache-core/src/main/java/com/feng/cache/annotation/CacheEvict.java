package com.feng.cache.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.feng.cache.config.CacheConfig;
import com.feng.cache.key.KeyGenerator;
import org.springframework.core.annotation.AliasFor;

/**
 *
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface CacheEvict {

    /**
     * 别名是 {@link #cacheName}.
     *
     * @return String[]
     */
    @AliasFor("cacheName")
    String value() default CacheConfig.DEFAULT_CACHE_NAME;

    /**
     * 缓存名称，支持SpEL表达式
     *
     * @return String[]
     */
    @AliasFor("value")
    String cacheName() default CacheConfig.DEFAULT_CACHE_NAME;

    /**
     * 缓存key，支持SpEL表达式
     * <p>The SpEL expression evaluates against a dedicated context that provides the
     * following meta-data:
     * <ul>
     * <li>{@code #root.method}, {@code #root.target}, and {@code #root.caches} for
     * references to the {@link java.lang.reflect.Method method}, target object, and
     * affected cache(s) respectively.</li>
     * <li>Shortcuts for the method name ({@code #root.methodName}) and target class
     * ({@code #root.targetClass}) are also available.
     * <li>Method arguments can be accessed by index. For instance the second argument
     * can be accessed via {@code #root.args[1]}, {@code #p1} or {@code #a1}. Arguments
     * can also be accessed by name if that information is available.</li>
     * </ul>
     *
     * @return String
     */
    String key() default "";

    /**
     * 自定义KeyGenerator的beanname
     */
    String keyGenerator() default "";

    /**
     * 是否删除所有缓存,为true时key属性无效
     */
    boolean allEntries() default false;

    /**
     * 是否在目标方法之前清除缓存,为ture则目标方法调用前后都会删除缓存
     */
    boolean beforeInvocation() default false;

    /**
     * 目标方法之后清除缓存时的等待时间，单位：毫秒
     * @return
     */
    long waittime() default 20;

    /**
     * 可以精确定位是清除哪个cache，值为方法名
     * 支持 方法名 和 全类名.方法名 这两种格式
     * 如果仅仅只有方法名，默认会加上当前类的全限定类名
     * 如果此属性有值，则优先级最高，如果根据该值找不到对饮的Cache,则不删除缓存
     * @return
     */
    String targetCache() default "";


}

