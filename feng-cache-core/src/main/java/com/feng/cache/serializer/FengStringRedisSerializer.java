package com.feng.cache.serializer;

import com.alibaba.fastjson.JSONObject;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.util.Assert;

import java.nio.charset.Charset;

public class FengStringRedisSerializer implements RedisSerializer<Object> {

    private final Charset charset;

    public FengStringRedisSerializer() {
        this(Charset.forName("UTF8"));
    }

    public FengStringRedisSerializer(Charset charset) {
        Assert.notNull(charset, "Charset must not be null!");
        this.charset = charset;
    }

    public String deserialize(byte[] bytes) {
        return (bytes == null ? null : new String(bytes, charset));
    }

    public byte[] serialize(Object obj) {
//        return (obj == null ? null : obj.toString().getBytes(charset));
        if(obj instanceof String) return String.valueOf(obj).getBytes(charset);
        return (obj == null ? null : JSONObject.toJSONString(obj)
                .replace("\"","")
                .replace(":","-")
                .getBytes(charset));
    }
}
