package com.feng.cache.config;


import com.feng.cache.config.single.CaffeineCacheConfig;
import com.feng.cache.config.single.RedisCacheConfig;
import org.springframework.data.redis.core.TimeoutUtils;


public class MultiCacheConfig extends CacheConfig{

    //是否使用CaffeineCache
    private boolean useCaffeineCache = true;

    private CaffeineCacheConfig caffeineCacheConfig;

    private RedisCacheConfig redisCacheConfig;

    public boolean isUseCaffeineCache() {
        return useCaffeineCache;
    }

    public void setUseCaffeineCache(boolean useCaffeineCache) {
        this.useCaffeineCache = useCaffeineCache;
    }

    public CaffeineCacheConfig getCaffeineCacheConfig() {
        return caffeineCacheConfig;
    }

    public void setCaffeineCacheConfig(CaffeineCacheConfig caffeineCacheConfig) {
        this.caffeineCacheConfig = caffeineCacheConfig;
    }

    public RedisCacheConfig getRedisCacheConfig() {
        return redisCacheConfig;
    }

    public void setRedisCacheConfig(RedisCacheConfig redisCacheConfig) {
        this.redisCacheConfig = redisCacheConfig;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(this.cacheName+":multi:");
        sb.append("useCaffeineCache=").append(this.useCaffeineCache)
                .append(",redisCache=").append(this.redisCacheConfig.toString());
                if(this.useCaffeineCache){
                    sb.append(",caffeineCache=").append(this.caffeineCacheConfig.toString());
                }
        return sb.toString();
    }
}
