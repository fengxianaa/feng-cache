package com.feng.cache.config;

public class CacheConfig {

    public static final String DEFAULT_CACHE_NAME="fengCache";

    //缓存名称
    protected String cacheName;
    //缓存描述
    protected String depict;

    public String getCacheName() {
        return cacheName;
    }

    public void setCacheName(String cacheName) {
        this.cacheName = cacheName;
    }

    public String getDepict() {
        return depict;
    }

    public void setDepict(String depict) {
        this.depict = depict;
    }

}
