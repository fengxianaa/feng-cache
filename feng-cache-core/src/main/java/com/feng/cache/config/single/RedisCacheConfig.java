package com.feng.cache.config.single;


import org.springframework.data.redis.core.TimeoutUtils;

import java.util.concurrent.TimeUnit;

/**
 * redis缓存配置类，对应@RedisCache注解
 */
public class RedisCacheConfig extends SingleCacheConfig{

    /**
     * 是否使用缓存名称作为 redis key 前缀
     * 如果为true,方便在redis界面化客户端查看缓存数据
     */
    private boolean usePrefix = true;

    /**
     * 非空值和null值之间的时间倍率，allowNullValue=true才有效
     * <p>
     * 如配置缓存的有效时间是200秒，倍率这设置成10，
     * 那么当缓存value为null时，缓存的有效时间将是20秒，非空时为200秒
     * </p>
     */
    private int magnification;

    public boolean isUsePrefix() {
        return usePrefix;
    }

    public void setUsePrefix(boolean usePrefix) {
        this.usePrefix = usePrefix;
    }

    public int getMagnification() {
        return magnification;
    }

    public void setMagnification(int magnification) {
        this.magnification = magnification;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(this.cacheName+":redis:");
        sb.append("usePrefix=").append(this.usePrefix)
                .append(",magnification=").append(this.magnification)
                .append(",expireTime=").append(TimeoutUtils.toSeconds(this.expireTime,this.timeUnit))
                .append(",allowNullValue=").append(this.allowNullValue);
        return sb.toString();
    }
}
