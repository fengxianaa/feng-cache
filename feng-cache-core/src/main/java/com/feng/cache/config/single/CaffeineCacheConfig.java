package com.feng.cache.config.single;

import com.feng.cache.support.ExpireMode;
import org.springframework.data.redis.core.TimeoutUtils;

import java.util.concurrent.TimeUnit;

public class CaffeineCacheConfig extends SingleCacheConfig{

    //缓存初始化大小
    private int initialCapacity;
    //缓存最大Size
    private int maximumSize;
    /**
     * 缓存失效模式
     * @see ExpireMode
     */
    private ExpireMode expireMode;

    public int getInitialCapacity() {
        return initialCapacity;
    }

    public void setInitialCapacity(int initialCapacity) {
        this.initialCapacity = initialCapacity;
    }

    public int getMaximumSize() {
        return maximumSize;
    }

    public void setMaximumSize(int maximumSize) {
        this.maximumSize = maximumSize;
    }

    public ExpireMode getExpireMode() {
        return expireMode;
    }

    public void setExpireMode(ExpireMode expireMode) {
        this.expireMode = expireMode;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(this.cacheName+":caffeine:");
        sb.append("initialCapacity=").append(this.initialCapacity)
                .append(",maximumSize=").append(this.maximumSize)
                .append(",expireMode=").append(this.expireMode)
                .append(",expireTime=").append(TimeoutUtils.toSeconds(this.expireTime,this.timeUnit))
                .append(",allowNullValue=").append(this.allowNullValue);
        return sb.toString();
    }
}
