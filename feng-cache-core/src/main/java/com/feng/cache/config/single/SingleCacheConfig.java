package com.feng.cache.config.single;

import com.feng.cache.config.CacheConfig;

import java.util.concurrent.TimeUnit;

public class SingleCacheConfig extends CacheConfig {

    //缓存失效时间
    protected long expireTime;
    //缓存失效时间：单位
    protected TimeUnit timeUnit;

    //是否允许存储null值
    protected boolean allowNullValue;

    public long getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(long expireTime) {
        this.expireTime = expireTime;
    }

    public TimeUnit getTimeUnit() {
        return timeUnit;
    }

    public void setTimeUnit(TimeUnit timeUnit) {
        this.timeUnit = timeUnit;
    }

    public boolean isAllowNullValue() {
        return allowNullValue;
    }

    public void setAllowNullValue(boolean allowNullValue) {
        this.allowNullValue = allowNullValue;
    }
}
