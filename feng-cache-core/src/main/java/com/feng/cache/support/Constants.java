package com.feng.cache.support;

public interface Constants {

    String DELETE_CACHE_TOPIC = "DELETE_CACHE_TOPIC";
    String CLEAR_CACHE_TOPIC = "CLEAR_CACHE_TOPIC";
}
