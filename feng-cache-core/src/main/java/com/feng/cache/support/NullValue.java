package com.feng.cache.support;


import org.springframework.lang.Nullable;

import java.io.Serializable;

/**
 * Simple serializable class that serves as a {@code null} replacement
 * for cache stores which otherwise do not support {@code null} values.
 *
 */
public final class NullValue implements Serializable {

    public static final Object INSTANCE = new NullValue();

    private static final long serialVersionUID = 1L;

    private NullValue() {
    }


    public boolean equals(@Nullable Object obj) {
        return this == obj || obj == null;
    }

    public int hashCode() {
        return org.springframework.cache.support.NullValue.class.hashCode();
    }

    public String toString() {
        return "null";
    }

    private Object readResolve() {
        return INSTANCE;
    }
}
