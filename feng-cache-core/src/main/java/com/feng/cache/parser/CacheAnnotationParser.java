package com.feng.cache.parser;

import com.alibaba.fastjson.JSONObject;
import com.feng.cache.annotation.CacheEvict;
import com.feng.cache.annotation.Cacheable;
import com.feng.cache.annotation.CaffeineCache;
import com.feng.cache.annotation.RedisCache;
import com.feng.cache.aop.CacheAspect;
import com.feng.cache.config.CacheConfig;
import com.feng.cache.config.MultiCacheConfig;
import com.feng.cache.config.single.CaffeineCacheConfig;
import com.feng.cache.config.single.RedisCacheConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.AnnotatedElementUtils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Cache注解解析器，@CachePut、@Cacheable、@CacheEvict注解
 */
public class CacheAnnotationParser {
    protected static final Logger logger = LoggerFactory.getLogger(CacheAnnotationParser.class);

    private ConcurrentHashMap<String,CacheConfig> cacheConfigMap = new ConcurrentHashMap<>();

    public CacheConfig parserCacheAnnotatoins(Method method){
        /*
         * 1.从根据类名方法名从缓存中获取配置
         * 2.如果缓存中没有，用解析器解析注解，得到配置(CacheConfig)
         */
        String methodName = method.getDeclaringClass().getName() + "."+ method.getName();
        CacheConfig cacheConfig = cacheConfigMap.get(methodName);
        if(cacheConfig!=null) return cacheConfig;
        Cacheable cacheableAnnotation = AnnotatedElementUtils.getMergedAnnotation(method, Cacheable.class);
        if(cacheableAnnotation!=null){
            cacheConfig = getCacheConfig(cacheableAnnotation);
            cacheConfigMap.put(methodName,cacheConfig);
            return cacheConfig;
        }
        CacheEvict cacheEvictAnnotation = AnnotatedElementUtils.getMergedAnnotation(method, CacheEvict.class);
        if(cacheEvictAnnotation!=null){
            //获取targetCache属性，其实就是一个方法名
            String targetCacheKey = cacheEvictAnnotation.targetCache();
            if(!targetCacheKey.contains(".")) targetCacheKey = method.getDeclaringClass().getName()+"."+targetCacheKey;
            cacheConfig = cacheConfigMap.get(targetCacheKey);
            //解析@CacheEvict有可能返回的cacheConfig是null，因为在调用清除缓存的方法之前并没有调用过存储缓存的方法
            if(cacheConfig!=null) cacheConfigMap.put(methodName,cacheConfig);
            logger.info("根据 {} 找到的cacheConfig={}",targetCacheKey, JSONObject.toJSONString(cacheConfig));
            return cacheConfig;
        }
        //TODO




//        else {
//            throw new IllegalArgumentException("无法解析注解:{}，请提供 @CachePut、@Cacheable、@CacheEvict注解");
//        }
        return cacheConfig;
    }

    public CacheConfig parserCacheAnnotatoins(Annotation annotation){
        if(annotation instanceof Cacheable){
            return getCacheConfig((Cacheable)annotation);
        }else {
            throw new IllegalArgumentException("无法解析注解:{}，请提供 @CachePut、@Cacheable、@CacheEvict注解");
        }
    }

    private CacheConfig getCacheConfig(Cacheable cacheable) {
        MultiCacheConfig cacheConfig = new MultiCacheConfig();
        cacheConfig.setCacheName(cacheable.cacheName());
        cacheConfig.setDepict(cacheable.depict());
        cacheConfig.setUseCaffeineCache(cacheable.useCaffeineCache());
        //处理RedisCache
        RedisCacheConfig redisCacheConfig = new RedisCacheConfig();
        RedisCache redisCache = cacheable.RedisCache();
        redisCacheConfig.setCacheName(cacheConfig.getCacheName());
        redisCacheConfig.setDepict(cacheConfig.getDepict());
        redisCacheConfig.setUsePrefix(redisCache.usePrefix());
        redisCacheConfig.setExpireTime(redisCache.expireTime());
        redisCacheConfig.setTimeUnit(redisCache.timeUnit());
        redisCacheConfig.setAllowNullValue(redisCache.allowNullValue());
        redisCacheConfig.setMagnification(redisCache.magnification());
        cacheConfig.setRedisCacheConfig(redisCacheConfig);
        //处理CaffeineCache
        if(cacheable.useCaffeineCache()){
            CaffeineCacheConfig caffeineCacheConfig = new CaffeineCacheConfig();
            CaffeineCache caffeineCache = cacheable.CaffeineCache();
            caffeineCacheConfig.setCacheName(cacheConfig.getCacheName());
            caffeineCacheConfig.setDepict(cacheConfig.getDepict());
            caffeineCacheConfig.setExpireMode(caffeineCache.expireMode());
            caffeineCacheConfig.setInitialCapacity(caffeineCache.initialCapacity());
            caffeineCacheConfig.setMaximumSize(caffeineCache.maximumSize());
            caffeineCacheConfig.setAllowNullValue(caffeineCache.allowNullValue());
            caffeineCacheConfig.setExpireTime(caffeineCache.expireTime());
            caffeineCacheConfig.setTimeUnit(caffeineCache.timeUnit());
            cacheConfig.setCaffeineCacheConfig(caffeineCacheConfig);
        }
        return cacheConfig;
    }
}
