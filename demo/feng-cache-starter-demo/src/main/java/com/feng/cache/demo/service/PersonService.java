package com.feng.cache.demo.service;


import com.feng.cache.demo.entity.Person;

public interface PersonService {
    Person save(Person person);

    void remove(Long id);

    void removeAll();

    Person findOne(Person person);
}
