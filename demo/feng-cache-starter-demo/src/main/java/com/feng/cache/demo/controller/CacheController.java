package com.feng.cache.demo.controller;

import com.feng.cache.demo.entity.Person;
import com.feng.cache.demo.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CacheController {

    @Autowired
    PersonService personService;

    @RequestMapping("/put")
    public long put(@RequestBody Person person) {
        Person p = personService.save(person);
        return p.getId();
    }

    @RequestMapping("/get")
    public Person cacheable(@RequestBody Person person) {
        return personService.findOne(person);
    }

    @RequestMapping("/del")
    public String del(@RequestParam long id) {
        personService.remove(id);
        return "ok";
    }

}