package com.feng.cache.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
//@EnableCaching
public class FengCacheStartDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(FengCacheStartDemoApplication.class, args);
    }
}
